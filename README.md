# Python 3.9 test drive

## Conda

Tried Conda in Feb

```bash
conda create -n py39 python=3.9
conda activate py39
python try_py39.py
conda deactivate
```

## Docker

Tzdata didn't work with the standard build, so created a dockerfile to install it.

```bash
DOCKER_BUILDKIT=0 docker build . -t py39
docker run --rm -it py39

#optional, for trying stuff out...
docker run --rm -it py39 python
docker run --rm -it py39 sh
```

Also, in Docker, tried Distroless from Google, but the Python was 3.5.

