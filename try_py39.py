import sys
from datetime import date
from datetime import datetime
from zoneinfo import ZoneInfo

print(f"{sys.version_info[:] = }")

name = "David"
print(f"{name = }")

a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
lim = 10
if (n := len(a)) > lim:
    print(f"List is too long ({n = } elements, expected <= {lim})")

user = 'eric_idle'
member_since = date(1975, 7, 31)
print(f'{user=} {member_since=}')

x = {"key1": "value1 from x", "key2": "value2 from x"}
y = {"key2": "value2 from y", "key3": "value3 from y"}
print(x | y)
print(y | x)
z = x
z |= y
print(z)

def greet_all(names: list[str]) -> None:
    for name in names:
        print("Hello", name)

greet_all(['Alice', 'Bob', 'Charles', 4, 5.0])

str1 = 'prewordsuf'
print(str1.removeprefix('pre'))
print(str1.removesuffix('suf'))

# below didn't work in docker, needed to install tzdata in alpine
release = datetime(2020, 10, 5, 3, 9, tzinfo=ZoneInfo("America/Vancouver"))
print(release.astimezone(ZoneInfo("Europe/Oslo")))
